package com.yerbol.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CalculatorController {
  @GetMapping("/calculator")
  public String calculate(@RequestParam("a") Integer a, @RequestParam("b") Integer b,
      @RequestParam("operation") String operation, Model model) {

    String answer;
    switch (operation) {
    case "addition":
      answer = doAddition(a, b);
      break;
    case "subtraction":
      answer = doSubtration(a, b);
      break;
    case "multiplication":
      answer = doMultiplication(a, b);
      break;
    case "division":
      answer = doDivision(a, b);
      break;
    default:
      answer = "No such operation";
    }
    
    model.addAttribute("answer", answer);

    return "calculator/calculate";
  }

  private String doAddition(int a, int b) {
    return a + " + " + b + " = " + (a + b);
  }

  private String doSubtration(int a, int b) {
    return a + " - " + b + " = " + (a - b);
  }

  private String doMultiplication(int a, int b) {
    return a + " * " + b + " = " + (a * b);
  }

  private String doDivision(int a, int b) {
    if (b == 0) {
      return "divider can`t be 0";
    } else {
      return a + " / " + b + " = " + (1.0 * a / b);
    }
  }

}
